# Roomonitor Programmers test

This repo contains a test for a new programmers por Roomonitor

## The challenge

We have an ESP32 chipset, and you'll have to develop a proof of concept for:

* We have a MEMS omnidirectional microphone connected to D32 pin in the ESP 32. You need to get the "noise" measure in a range of 1 to 1024 each 1 minute the median of the values meassure it each 5 seconds.

* We have an MQTT server test.mosquitto.org in the port 1883, you need to connect the ESP32 to the wifi and to a mosquitto server after and send each 5 minutes a JSON string with the array of the noise measures in the topic "roomnoise/<ESP_Mac_id>"

* You have an RGB led connected to D4, when the ESP32 is disconnected to WIFI set the led in RED, when the Wifi is connected but not to MQTT set the led to BLUE, and when the ESP32 is connected to Wifi and MQTT and all is working, set the led in GREEN

* When you are not getting measures or sending values, the ESP32 must be in deep sleep mode.

* The firmware must be developed in C with Arduino framework

* The code must be organized in differents modules or files with each functionality